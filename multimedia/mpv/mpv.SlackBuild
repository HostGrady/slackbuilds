#!/bin/bash

# Slackware build script for mpv, modified by HostGrady

# Copyright 2015 John Vogel Corning, NY USA
# Copyright 2017,2018 (versions 0.25.0-0.29.1) Andreas Guldstrand
# Copyright 2020-2022 (0.29.1-0.34.1) Christoph Willing  Brisbane Australia
# Copyright 2022 (0.35+)HostGrady
#
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

cd $(dirname $0) ; CWD=$(pwd)

PRGNAM=mpv
VERSION=${VERSION:-0.35.1}
BUILD=${BUILD:-1}
TAG=${TAG:-_SBo}
PKGTYPE=${PKGTYPE:-tgz}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i586 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

if [ ! -z "${PRINT_PACKAGE_NAME}" ]; then
  echo "$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
  exit 0
fi

TMP=${TMP:-/tmp/SBo}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $PRGNAM-$VERSION
tar xvf $CWD/$PRGNAM-$VERSION.tar.gz
cd $PRGNAM-$VERSION
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;

DOCS="Copyright LICENSE.GPL LICENSE.LGPL README.md RELEASE_NOTES VERSION"
MANUAL="DOCS/*.md DOCS/*.rst DOCS/*.txt"

# This leaves an opening for additional build options.
BUILD_OPTS=${BUILD_OPTS:-""}

PKG_CONFIG_PATH="/usr/lib${LIBDIRSUFFIX}/ffmpeg4/pkgconfig"
export PKG_CONFIG_PATH

# based on portions from: https://slackbuilds.org/templates/meson-template.SlackBuild
# and, of course, from the original waf version

mkdir build
cd build

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \

# this says depcrecated, should probably fix at some point
# this (should be) identical to the waf build in regard to settings.
meson setup .. \
	--buildtype=release \
	--infodir=/usr/info \
	--libdir=/usr/lib${LIBDIRSUFFIX} \
	--localstatedir=/var \
	--mandir=/usr/man \
	--prefix=/usr \
	--sysconfdir=/etc \
	-Dlibmpv=true \
	-Dhtml-build=enabled \
	-Ddvdnav=enabled \
	-Dcdda=enabled \
	-Ddvbin=enabled \
	-Dstrip=true \
	$BUILD_OPTS
"${NINJA:=ninja}"
DESTDIR=$PKG $NINJA install
cd ..

# Don't ship .la files:
rm -f $PKG/{,usr/}lib${LIBDIRSUFFIX}/*.la

for f in $PKG/etc/mpv/*.conf ; do
  mv $f{,.new}
done

find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

if [ -d $PKG/usr/man ]; then
  find $PKG/usr/man -type f -exec gzip -9 {} \;
  for i in $( find $PKG/usr/man -type l ) ; do
    ln -s $( readlink $i ).gz $i.gz
    rm $i
  done
fi

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a $DOCS $PKG/usr/doc/$PRGNAM-$VERSION
mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION/manual
for f in $MANUAL; do
  cp -a $f $PKG/usr/doc/$PRGNAM-$VERSION/manual/
done
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE
